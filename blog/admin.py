from django.contrib import admin
from .models import Article, Category, IPAddress

#Admin header change
admin.site.site_header = "وبلاگ جنگو من"


# Register your models here.
def make_published(modeladmin, request, queryset):
    rows_updated = queryset.update(status='p')
    if rows_updated == 1:
        message_bit = "منتشر شد."
    else:
        message_bit = "منتشر شدند."
    modeladmin.message_user(request, "{} مقاله {}".format(rows_updated, message_bit))
make_published.short_description = "انتشار مقالات انتخاب شده"

def make_draft(modeladmin, request, queryset):
    rows_updated = queryset.update(status='d')
    if rows_updated == 1:
        message_bit = "پیش نویس شد."
    else:
        message_bit = "پیش نویس شدند."
    modeladmin.message_user(request, "{} مقاله {}".format(rows_updated, message_bit))
make_draft.short_description = "پیش نویس شدن مقالات انتخاب شده"


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'parent', 'status','position')
    list_filter = (['status'])
    search_fields = ('title', 'slug')
    prepopulated_fields = { 'slug':('title',) }

admin.site.register(Category, CategoryAdmin)

class ArtcleAdmin(admin.ModelAdmin):                                        #cause it's many-to-many field!
    list_display = ('title', 'thumbnail_tag', 'slug','author', 'jpublish','is_special', 'status', 'category_to_str')
    list_filter = ('publish', 'status', 'author')
    search_fields = ('title', 'description')
    prepopulated_fields = { 'slug':('title',) }
    ordering = ['status', '-publish']
    actions = [make_published, make_draft]

    #category is manyTomany field and we cannot add mTm field to list_display. So we use category_to_str to display it.
    def category_to_str(self, obj):
        return "، ".join([category.title for category in obj.category.active()])
    category_to_str.short_description = "دسته بندی"

admin.site.register(Article, ArtcleAdmin)
admin.site.register(IPAddress)